-- 1
SELECT customerName FROM customers WHERE country = "Philippines";

-- 2
SELECT customerName FROM customers WHERE country = "USA";

-- 3
SELECT contactLastName,contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

-- 4
SELECT productName,MSRP FROM products WHERE productName = "The Titanic";

-- 5
SELECT firstName,lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

-- 6
SELECT customerName FROM customers WHERE state IS NULL;

-- 7
SELECT firstName,lastName,email FROM employees WHERE firstName = "Steve" AND lastName = "Patterson";

-- 8
SELECT customerName,country,creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

-- 9
SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

-- 10
SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

-- 11
SELECT DISTINCT country FROM customers;

-- 12
SELECT DISTINCT status FROM orders;

-- 13
SELECT customerName,country FROM customers WHERE country IN ("USA","France","Canada");

-- 14
SELECT firstName,lastName,city FROM employees
    JOIN offices ON employees.officeCode = offices.officeCode
    WHERE city = "Tokyo";

-- 15
SELECT customerName FROM customers
    JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
    WHERE firstName = "Leslie" AND lastName = "Thompson";

-- 16
SELECT productName,quantityInStock FROM products WHERE productline = "planes" AND quantityInStock <1000;

-- 17
SELECT customerName FROM customers WHERE phone LIKE "%+81%";

-- 18
SELECT COUNT(*) FROM customers WHERE country = "UK";

-- STRETCH GOAL
-- 1
SELECT productName,customerName FROM products
    JOIN orderdetails ON products.productCode = orderdetails.productCode
    JOIN orders ON orderdetails.orderNumber = orders.orderNumber
    JOIN customers ON orders.customerNumber = customers.customerNumber
    WHERE customerName = "Baane Mini Imports";

-- 2
SELECT e.firstName,e.lastName FROM employees e
    INNER JOIN employees m ON m .employeeNumber = e.reportsTo
    WHERE e.reportsTo = 1143;

-- 3
SELECT productName,MAX(MSRP) FROM products WHERE MSRP;

-- 4
SELECT productline FROM products GROUP BY(productline);

-- 5
SELECT COUNT(*) FROM orders WHERE status = "Cancelled";

